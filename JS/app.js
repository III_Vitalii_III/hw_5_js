alert(`Перше завдання`);
let a = +prompt(`Введіть перше число`);
while (isNaN(a)) {
    alert(`Неправильно введене перше число`);
    a = +prompt(`Введіть перше число`);
}

let b = +prompt(`Введіть друге число`);
while (isNaN(b) || b === 0) {
    alert(`Неправильно введене друге число`);
    b = +prompt(`Введіть друге число`);
}

function divide(a, b) {
    return a / b;
}

console.log(divide(a, b));
console.log(`___________________________________________________________________________`);

alert(`Друге завдання`);

let firstNum = +prompt(`Введіть перше число`);
let secondNum = +prompt(`Введіть друге число`);

while (isNaN(firstNum) || isNaN(secondNum)) {
    alert(`Ви ввели не число, будь ласка, спробуйте ще раз`);
    firstNum = +prompt(`Введіть перше число`);
    secondNum = +prompt(`Введіть друге число`);
}

let mathOperat = prompt(`Введіть операцію: +, -, *, /`);
while (mathOperat !== `+` && mathOperat !== `-` && mathOperat !== `*` && mathOperat !== `/`) {
    mathOperat = prompt(`Ви ввели неправильну операцію, будь ласка, спробуйте ще раз: +, -, *, /`);
}

function calculate(num1, num2, operation) {
    switch (operation) {
        case `+`:
            return num1 + num2;
        case `-`:
            return num1 - num2;
        case `*`:
            return num1 * num2;
        case `/`:
            return num2 !== 0 ? num1 / num2 : `На нуль ділити не можна!`;
        default:
            return `Невідома операція`;
    }
}

const result = calculate(firstNum, secondNum, mathOperat);
console.log(result);

console.log(`___________________________________________________________________________`);

alert(`Третє завдання`);

let number = +prompt(`Введіть число`);
while (isNaN(number) || number === 0) {
    number = +prompt(`Введіть число ще раз`);
}

const functionFactorial = (enterNum) => {
    if (enterNum === 1) {
        return 1;
    } else {
        return enterNum * functionFactorial(enterNum - 1);
    }
}

const factorial = functionFactorial(number);
alert(factorial);
